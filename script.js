//Criar o layout do jogo
let matriz = [[], [], [], [], [], [], []];
let count = 0;
let selectedColumn;
let player = 1;
let celulaDisco = [];

function createGameDivs() {
  let game = document.getElementById("game");
  let squareId = 0;
  let columnId = 0;
  for (let coluna = 0; coluna < 7; coluna++) {
    let column = document.createElement("div");
    game.appendChild(column);
    column.className = "column";
    column.id = `${columnId}`;
    column.addEventListener("click", function () {
      (selectedColumn = column), createDisc();
    });

    for (let linha = 0; linha < 6; linha++) {
      matriz[linha][coluna] = 0;
      let square = document.createElement("div");
      column.appendChild(square);
      square.className = "square";
      square.id = `${squareId}`;
      squareId++;
    }
    columnId++;
  }
}
let lastDiscAdd;
createGameDivs();

function createDisc() {
  for (let index = 5; index >= 0; index--) {
    if (selectedColumn.childNodes[0].childElementCount > 0) {
      window.alert("Coluna cheia");
      break;
    }

    if (selectedColumn.childNodes[index].childElementCount == 0) {
      let disc = document.createElement("div");
      selectedColumn.childNodes[index].appendChild(disc);
      disc.className = "disc";

      disc.id = playerId();
      savePosition(Number(selectedColumn.id), index);
      lastDiscAdd = selectedColumn.childNodes[index];
      count++;
      condicaoVitoria();
      alertaCondicoes();
      break;
    }
    }
}

function playerId() {
  if (count % 2 == 0) {
    player = 1;
    return "player1";
  } else {
    player = 2;
    return "player2";
  }
}

function savePosition(x, y) {
  matriz[y][x] = player;
  console.log(y, x);
}

let condVitoria = false;
let valorEmpate = false;

function condicaoVitoria() {
  for (let y = 0; y < limiteEixoY; y++) {
    for (let x = 0; x < limiteEixoX; x++) {
      condEmpate();
      vitoriaCondHorizontal(x, y);
      vitoriaCondVertical(x, y);
      vitoriaCondDiagonal1(x, y);
      vitoriaCondDiagonal2(x, y);
    }
  }
}

function alertaCondicoes() {
  setTimeout(function () {
    if (valorEmpate) {
      mostrarMensagem();
    }
    if (condVitoria) {
      mostrarMensagem()
    }
  }, 500);
}
//limite de eixos
let limiteEixoX = matriz[0].length;
let limiteEixoY = matriz.length;

//condição horizontal

function vitoriaCondHorizontal(x, y) {
  if (
    player === matriz[y][x] &&
    player === matriz[y][x + 1] &&
    player === matriz[y][x + 2] &&
    player === matriz[y][x + 3]
  ) {
    console.log("condição de vitoria horizontal");
    condVitoria = true;
  }
}

//confição vertical

function vitoriaCondVertical(x, y) {
  if (
    player === matriz[y][x] &&
    player === matriz[y + 1][x] &&
    player === matriz[y + 2][x] &&
    player === matriz[y + 3][x]
  ) {
    console.log("condição de vitoria vertical");
    condVitoria = true;
  }
}

function vitoriaCondDiagonal1(x, y) {
  if (
    player === matriz[y][x] &&
    player === matriz[y + 1][x + 1] &&
    player === matriz[y + 2][x + 2] &&
    player === matriz[y + 3][x + 3]
  ) {
    console.log("condição de vitoria diagonal1");
    condVitoria = true;
  }
}

function vitoriaCondDiagonal2(x, y) {
  if (y <= 3) {
    return;
  }
  if (
    player === matriz[y][x] &&
    player === matriz[y - 1][x + 1] &&
    player === matriz[y - 2][x + 2] &&
    player === matriz[y - 3][x + 3]
  ) {
    console.log("condição de vitoria diagonal2");
    condVitoria = true;
  }
}

//condição empate
function condEmpate() {
  valorEmpate = !JSON.stringify(matriz).includes("0");
}

function mostrarMensagem(){
    let pUser = document.getElementById('pUser')
    let color = ''
    if (player == 1){
        color = 'azul'
    }else if (player == 2){
        color = 'vermelho'
    }
    let modal = document.getElementById('modal')
    modal.style.display = 'inherit'
    pUser.innerHTML = `Jogador  ${color}  venceu`
    if(valorEmpate){
        pUser.innerHTML = `O jogo empatou!` 
    }
}

function refresh(){
    window.location.reload()
}